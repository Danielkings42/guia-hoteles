$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#hotelFilipinas').on('show.bs.modal', function (e) {
      console.log('El modal se está mostrando');
      $('#hotelFilipinasBtn').removeClass('btn-success');
      $('#hotelFilipinasBtn').addClass('btn-secondary');
      $('#hotelFilipinasBtn').prop('disabled', true);
    });
    $('#hotelFilipinas').on('hide.bs.modal', function (e) {
      console.log('El modal se está escondiendo');
    });
    $('#hotelFilipinas').on('hidden.bs.modal', function (e) {
      console.log('El modal se escondió');
      $('#hotelFilipinasBtn').removeClass('btn-secondary');
      $('#hotelFilipinasBtn').addClass('btn-success');
      $('#hotelFilipinasBtn').prop('disabled', false);
    });
    $('#hotelFilipinas').on('shown.bs.modal', function (e) {
      console.log('El modal se mostró');
    });
  });